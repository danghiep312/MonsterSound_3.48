﻿using System.Collections;
#if UNITY_IOS
using Balaso;
using com.adjust.sdk;
#endif
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Example MonoBehaviour class requesting iOS Tracking Authorization
/// </summary>
public class AppTrackingListenner : MonoBehaviour
{
    public static AppTrackingListenner Instance;
    private void Awake()
    {
        Instance = this;
#if UNITY_IOS
        Debug.Log("Reg ad network");
        AppTrackingTransparency.RegisterAppForAdNetworkAttribution();
        AppTrackingTransparency.UpdateConversionValue(3);
#endif
    }

    private int countingATT;

    void Start()
    {
#if UNITY_ANDROID
        gameObject.SetActive(false);
#endif
        // Load counting ATT
        countingATT = PlayerPrefs.GetInt("CountingATT", 0);
        ShowPopUp();
    }
    public bool CheckVersionAbove145()
    {
        if (isTest) return true;
#if UNITY_IOS
        try
        {
            string version = UnityEngine.iOS.Device.systemVersion;
            string[] splits = version.Split('.');
            Debug.Log("Version: " + version);
            if (int.Parse(splits[0]) >= 14)
            {
                if (int.Parse(splits[0]) >= 15)
                {
                    return true;
                }
                if (int.Parse(splits[1]) >= 5)
                {
                    return true;
                }
            }
        }
        catch (System.Exception e)
        {
            Debug.LogError(e);

        }
#endif
        return false;
    }
    [SerializeField]
    IOSTrackingPanel iosTrackingPanel;
    [SerializeField]
    IOSTrackingReminderPanel iosReminderPanel;

    public void ShowPopUp()
    {
#if UNITY_IOS
        Debug.Log("Show popup here");
        if (!IsAllow())
        {
            if (PlayerPrefs.GetInt("ios14", 0) == 0)
            {
                isFirst = true;
                PlayerPrefs.SetInt("ios14", 1);
                iosTrackingPanel.SetUp();
            }
            else
            {

                Adjust.Instance.StartManual();
                Debug.Log("CHECK STATUS " + Time.time + " " + lastTime);
                if (Time.time - lastTime > 5 * 60)
                {
                    if (countingATT < 1)
                    {
                        PlayerPrefs.SetInt("CountingATT", ++countingATT);
                        isFirst = false;
                        Debug.Log("Reminder ATT show!!!");
                        iosReminderPanel.SetUp();
                        lastTime = Time.time;
                    }
                }

            }

        }
        else
        {
            if (PlayerPrefs.GetInt("ATT", 0) == 0)
            {
                PlayerPrefs.SetInt("ATT", 1);
                MasterControl.Instance.InitAds();
            }
            Adjust.Instance.StartManual();
        }
#endif
    }
    bool isFirst = true;
    public void ShowAskPanel()
    {
#if UNITY_IOS
        Debug.Log("SHOW ASK PANEL");
        if (!IsAllow())
        {
            Debug.Log("Requesting authorization...");
            AppTrackingTransparency.OnAuthorizationRequestDone += OnAuthorizationRequestDone;
            AppTrackingTransparency.RequestTrackingAuthorization();

        }
#endif
    }
    float lastTime = 0, lastPlayCount = 0;

    [SerializeField]
    private bool isTest = false;
    public bool IsAllow()
    {
        if (isTest) return false;
#if UNITY_IOS
        if (CheckVersionAbove145())
        {
            AppTrackingTransparency.AuthorizationStatus currentStatus = AppTrackingTransparency.TrackingAuthorizationStatus;
            Debug.Log(string.Format("Current authorization status: {0}", currentStatus.ToString()));
            return (currentStatus == AppTrackingTransparency.AuthorizationStatus.AUTHORIZED);
        }
        else
        {
            
            return true;
        }
#endif
        return true;
    }
#if UNITY_IOS

    /// <summary>
    /// Callback invoked with the user's decision
    /// </summary>
    /// <param name="status"></param>
    private void OnAuthorizationRequestDone(AppTrackingTransparency.AuthorizationStatus status)
    {
        switch (status)
        {
            case AppTrackingTransparency.AuthorizationStatus.NOT_DETERMINED:
                Debug.Log("AuthorizationStatus: NOT_DETERMINED");
                if (!isFirst)
                    lastTime = -999999;
                break;
            case AppTrackingTransparency.AuthorizationStatus.RESTRICTED:
                Debug.Log("AuthorizationStatus: RESTRICTED");
                if (!isFirst)
                    lastTime = -999999;
                break;
            case AppTrackingTransparency.AuthorizationStatus.DENIED:
                Debug.Log("AuthorizationStatus: DENIED");
                if (!isFirst)
                    lastTime = -999999;
                break;
            case AppTrackingTransparency.AuthorizationStatus.AUTHORIZED:
                Debug.Log("AuthorizationStatus: AUTHORIZED");
                break;
        }
        MasterControl.Instance.InitAds();
        Adjust.Instance.StartManual();
        // Obtain IDFA
        Debug.Log(string.Format("IDFA: {0}", AppTrackingTransparency.IdentifierForAdvertising()));
        iosTrackingPanel.Deactive();
    }
#endif
}

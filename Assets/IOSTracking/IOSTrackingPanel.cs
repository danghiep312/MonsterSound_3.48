﻿using System.Collections;
using System.Collections.Generic;
using com.adjust.sdk;
using SourceAds;
using UnityEngine;

public class IOSTrackingPanel : PanelAbstract
{
    public static IOSTrackingPanel Instance;
    public override void PostInit()
    {
        Instance = this;
    }
    public override void Deactive()
    {
        gameObject.SetActive(false);
    }

    public void ClosePanel()
    {
        Animator anim = GetComponent<Animator>();
        if (anim != null)
        {
            anim.SetBool("Close", true);
        }
    }
    public void SetUp()
    {
        isSettingsOpenned = false;
        gameObject.SetActive(true);
        PlayerPrefs.SetInt("ATT", 1);
    }
    public void ShowDiaglog()
    {
        isSettingsOpenned = true;
        AppTrackingListenner.Instance.ShowAskPanel();
    }
    bool isSettingsOpenned;
    private void OnApplicationFocus(bool focus)
    {
        Debug.Log("ON FOCUS CHECK " + focus);
        if (isSettingsOpenned)
        {
            if (focus)
            {
                // if (AppTrackingListenner.Instance.IsAllow())
                // {
                //     FirebaseManager.Instance.LogEvent("ATAllow_" + FirebaseManager.GetLevel(PlayerPrefs.GetInt("ATTPopupCount", 0)) + "_Allow");
                // }
                // else
                // {
                //     FirebaseManager.Instance.LogEvent("ATAllow_" + FirebaseManager.GetLevel(PlayerPrefs.GetInt("ATTPopupCount", 0)) + "_NotAllow");
                // }
                Adjust.Instance.StartManual();
                gameObject.SetActive(false);

            }
        }


    }

}

﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using SourceAds;
using UnityEngine;

public class IOSTrackingReminderPanel : PanelAbstract
{
    public static IOSTrackingReminderPanel Instance;

    public override void PostInit()
    {
        Instance = this;
    }
    public override void Deactive()
    {
        gameObject.SetActive(false);
    }

    public  void ClosePanel()
    {
        Animator anim = GetComponent<Animator>();
        if (anim != null)
        {
            anim.SetBool("Close", true);
        }
    }
    bool isSettingsOpenned = false;
    public void OpenSettings()
    {
        Application.OpenURL("app-settings:");
        isSettingsOpenned = true;
    }
    public void SetUp()
    {
        isSettingsOpenned = false;
        PlayerPrefs.SetInt("ATTPopupCount", PlayerPrefs.GetInt("ATTPopupCount", 0) + 1);
        Show();
    }
    bool isBackToSetting = false;
    private void OnApplicationFocus(bool focus)
    {
        Debug.Log("ON FOCUS SETTINGS CHECK "+focus);
        if (isSettingsOpenned)
        {
            if (focus)
            {
                // if (AppTrackingListenner.Instance.IsAllow())
                // {
                //     FirebaseManager.Instance.LogEvent("ATAllow_" + FirebaseManager.GetLevel(PlayerPrefs.GetInt("ATTPopupCount", 0)) + "_Allow");
                // }
                // else
                // {
                //     FirebaseManager.Instance.LogEvent("ATAllow_" + FirebaseManager.GetLevel(PlayerPrefs.GetInt("ATTPopupCount", 0)) + "_NotAllow");
                // }

                gameObject.SetActive(false);

            }
        }
      

    }

}

﻿using UnityEngine;

public abstract class PanelAbstract : MonoBehaviour
{
    protected Animator _animator;
    protected Transform _transform;
    protected bool overrideBack = false;
    public virtual void Show()
    {
        Active();
    }
    public virtual void Hide()
    {
        _animator.SetTrigger("Close");
        Invoke("Deactive", 0.5f);
    }
    public virtual void Deactive()
    {
        gameObject.SetActive(false);
    }
    public virtual void Active()
    {
        gameObject.SetActive(true);
        overrideBack = false;
    }
    public void Init()
    {
        _animator = GetComponent<Animator>();
        _transform = transform;
        PostInit();
    }
    public virtual void ShowAfterAd() { }
    public virtual void Close()
    {
        Hide();
    }
    public abstract void PostInit();

    public virtual void OnBack()
    {
        Close();
    }
}
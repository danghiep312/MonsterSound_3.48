using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class MatchSound : MonoBehaviour
{
    public DataMonster[] dats;

    public AudioClip[] clips;

    [Button("Match")]
    public void Match()
    {
        foreach (var dat in dats)
        {
            var shortName = Util.RemoveSpace(dat.monsterName);
            var clip = Array.Find(clips, clip => shortName.Equals(Util.RemoveSpace(clip.name), StringComparison.OrdinalIgnoreCase));
            if (clip == null)
            {
                Debug.LogWarning("Not found clip: " + shortName);
                continue;
            }
            dat.clip = clip;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

public class RenameSound : MonoBehaviour
{
    public DataMonster[] dats;

    public AudioClip[] clips;

    [Button("Rename")]
    public void Rename()
    {
        for (int i = 0; i < dats.Length; i++)
        {
            dats[i].clip.name = dats[i].monsterName;
            //AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(dats[i].clip), dats[i].clip.name);
            if (dats[i].iosClip == null)
            {
                Debug.LogWarning("Not found ios clip: " + dats[i].monsterName);
                continue;
                
            }
//            Debug.Log(AssetDatabase.GetAssetPath(dats[i].iosClip));
            dats[i].iosClip.name = Util.RemoveSpace(dats[i].monsterName + "_ios");
            
            //AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(dats[i].iosClip), dats[i].iosClip.name);
            
        }
    }

    [Button("Rename Skibidi")]
    public void RenameSkibidi()
    {
        for (int i = 0; i < dats.Length; i++)
        {
            if (dats[i].monsterName.Contains("Skibidi"))
            {
                dats[i].monsterName = dats[i].monsterName.Replace("Skibidi", "Skibydy");
                string fileName = dats[i].name;
                if (fileName.Contains("Skibidi"))
                {
                    fileName = fileName.Replace("Skibidi", "Skibydy");
                }
                //AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(dats[i]), fileName);
            }
        }
    }

    [Button("Rename Object")]
    public void RenameObject()
    {
        for (int i = 0; i < dats.Length; i++)
        {
            //AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(dats[i]), dats[i].monsterName);
        }
    }
}

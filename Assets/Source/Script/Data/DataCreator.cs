using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class DataCreator : MonoBehaviour
{
#if UNITY_EDITOR

    public List<Monster> monsters;
    public int start;
    [Button("Create Data")]
    public void CreateData()
    {
        for (int i = 0; i < monsters.Count; i++)
        {
            var data = ScriptableObject.CreateInstance<DataMonster>();
            data.id = i + start;
            data.monsterName = monsters[i].name.Trim();
            data.mainMonsterSprite = monsters[i].mainSprite;
            data.iconMonsterSprite = monsters[i].mainSprite;
            data.clip = monsters[i].clip;
            
            AssetDatabase.CreateAsset(data, $"Assets/Source/Resources_moved/Data/Monsters Haloween/Monster1808/{data.monsterName}.asset");
        }
        AssetDatabase.SaveAssets();
    }
    
    private int GetIndexOfSprite(Sprite[] sprites, string name)
    {
        for (int i = 0; i < sprites.Length; i++)
        {
            if (sprites[i].name == name)
            {
                return i;
            }
        }

        return -1;
    }


    
#endif
}

[Serializable]
public class Monster
{
    public string name;
    public Sprite mainSprite;
    public AudioClip clip;
    
}

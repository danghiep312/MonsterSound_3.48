﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class DataHolder : MonoBehaviour
{
#if UNITY_EDITOR
    public List<DataMonster> dataMonsters;

    [Button]
    public void Sort()
    {
        dataMonsters.Sort((x, y) => x.id.CompareTo(y.id));
    }
#endif
}
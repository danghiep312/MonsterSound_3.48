using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "DataMonster", menuName = "Assets/DataMonster")]
public class DataMonster : ScriptableObject
{
    public int id;
    public string monsterName;
    [PreviewField]
    public Sprite mainMonsterSprite;
    [PreviewField]
    public Sprite iconMonsterSprite;
    [PreviewField]
    public Sprite cardMonsterSprite;
    
    public int adToUnlock;
    public Color textColor = Color.white;

    public AudioClip clip;
    public AudioClip iosClip;
}
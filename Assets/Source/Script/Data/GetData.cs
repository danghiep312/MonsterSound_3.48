using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Object = UnityEngine.Object;


public class GetData : MonoBehaviour
{
    public DataMonster[] data;
    [Button("Get Name")]
    public void GetAllName()
    {
        data = Resources.LoadAll<DataMonster>("Data/Monsters");
        Array.Sort(data, delegate(DataMonster o, DataMonster o1) { return o.id.CompareTo(o1.id);});
        string s = "";
        foreach (var dat in data)
        {
            s += dat.monsterName + "\n";
        }

        Debug.Log(s);
    }
}

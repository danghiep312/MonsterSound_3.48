﻿
using System;
using Firebase.RemoteConfig;
using UnityEngine;

public class ExcludeMonster
{
    public static void FetchCustomData()
    {
        Debug.Log("FetchData Custom");
        FirebaseRemoteConfig.DefaultInstance.ActivateAsync();
        var remoteConfig = FirebaseRemoteConfig.DefaultInstance;
        
        ConfigValue excludeId = remoteConfig.GetValue(CustomPattern.ExcludeIdMonster);
        Debug.Log("Exclude_id_monster: " + excludeId.StringValue);
        // ExcludeIdMonster = Array.ConvertAll(excludeId.StringValue.Replace(" ", "").Split(","), int.Parse);
        if (!string.IsNullOrEmpty(excludeId.StringValue))
        {
            PlayerPrefs.SetString(CustomPattern.ExcludeIdMonster, excludeId.StringValue.Replace(" ", ""));
        }
    }
}


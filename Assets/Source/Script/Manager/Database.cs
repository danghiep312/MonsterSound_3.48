using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;


public class Database : Singleton<Database>
{
   // public int loadDone;
    private AsyncOperationHandle<GameObject> _btHandle;
    public List<DataMonster> dataMonsters;
    public bool loadDone;


    [Button("Sort Monster")]
    public void SortMonster(List<DataMonster> list)
    {
        list.Sort((x, y) => x.id.CompareTo(y.id));
    }


    private IEnumerator LoadDataMonster(string key)
    {
        AsyncOperationHandle handle = Addressables.LoadAssetsAsync<DataMonster>(key, (dat) =>
        {
            dataMonsters.Add(dat);
            //Debug.Log("Load: " + dat.monsterName + " " + dat.id + " " + Time.time);
        });

        yield return handle;
        loadDone = true;
    }
    
    
    public List<DataMonster> GetMonstersData()
    {
        //dataMonsters.Sort((x, y) => x.id.CompareTo(y.id));
        return dataMonsters;
    }
    
    

    [Button]
    public void GetInfoId()
    {
        List<int> list = new List<int>();
        foreach (var data in dataMonsters)
        {
            list.Add(data.id);
        }
        list.Sort();
        string usedId = "";
        foreach (var i in list)
        {
            usedId += i + " ";
        }
        Debug.Log("used id: " + usedId);
        string unusedId = "";
        for (int i = 1; i < list[^1]; i++)
        {
            if (!list.Contains(i))
            {
                unusedId += i + " ";
            }
        }
        Debug.Log("unused id: " + unusedId);
    }

    #if UNITY_EDITOR
    [Button]
    public void MergeDataHolder()
    {
        dataMonsters.Clear();
        foreach (DataHolder holder in GetComponents<DataHolder>())
        {
            var dats = holder.dataMonsters;
            foreach (var monster in dats) {
                if (dataMonsters.Contains(monster)) continue;
                dataMonsters.Add(monster);
            }
            //dataMonsters.AddRange(holder.dataMonsters);
        }
    }
    #endif
}

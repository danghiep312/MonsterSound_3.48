﻿public enum EventID
{
    None = 0,
    GoToPlay,
    BackToMenu,
    OpenSetting,
    CloseLoading,
    Play,
    CloseSetting,
    OpenPopup,
    ClosePopup,
    RequestComplete,
    ClosePanel
}
using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GoogleMobileAds.Api;
using Sirenix.OdinInspector;
using UnityEngine;
using SourceAds;
using UnityEngine.SceneManagement;

public class GameController : Singleton<GameController>
{
    public bool vibrationOn;
    public bool flashOn;

    private float holdTime;

    public ParticleSystem wave;
    public bool nativeClick;

    private void Start()
    {
        Application.targetFrameRate = 90;
        vibrationOn = PlayerPrefs.GetInt("Vibration", 1) == 1;
        flashOn = PlayerPrefs.GetInt("Flash", 0) == 1;

        if (!PrefAccess.FirstTimePlay())
        {
            if (!SceneManager.GetActiveScene().name.Equals("Loading"))
            {
                ShowBanner();
            }
        }

        this.RegisterListener(EventID.GoToPlay, (param) => OnGoToPlay());
        this.RegisterListener(EventID.BackToMenu, (param) => OnBackToMenu());
    }

    private void OnBackToMenu()
    {
        ShowBanner();
    }

    private void OnGoToPlay()
    {
        HideBanner();
    }

    private void Update()
    {
        if (Time.frameCount % 60 == 0)
        {
            UIManager.Instance.SetInternetPanel(!CheckInternet());
        }
    }


    #region Ads

    public void ShowBanner()
    {
        try
        {
            MasterControl.Instance.ShowBanner();
        }
        catch
        {
            //ignored
        }
    }

    public async void ShowInter(bool allowDelay = false, Action<bool> callback = null, Action mustRunCallback = null)
    {
        if (PrefAccess.FirstTimePlay())
        {
            callback?.Invoke(true);
            mustRunCallback?.Invoke();
            return;
        }


        callback += (b) => mustRunCallback?.Invoke();
        if (MasterControl.Instance.IsInterstitialAdAvailable())
        {
            HideBanner();
            if (MasterControl.Instance.IsAllowShowAdLoadingPanel(AdType.Interstitial))
            {
                BgAdBlock.Instance.Show("Ad loading ...");
                callback += (b) => BgAdBlock.Instance.Hide();
                await UniTask.Delay(TimeSpan.FromSeconds(1f));
            }
        }

        var isShow = MasterControl.Instance.ShowInterstitialAd(allowDelay, callback);
        if (!isShow) mustRunCallback?.Invoke();
    }


    public async void ShowReward(Action<bool> callback = null, bool checkNoAds = false)
    {
        if (MasterControl.Instance.IsRewardAdAvailable())
        {
            HideBanner();
            if (MasterControl.Instance.IsAllowShowAdLoadingPanel(AdType.Reward))
            {
                Debug.Log("Show ad loading");
                BgAdBlock.Instance.Show("Ad loading ...");
                callback += (b) => BgAdBlock.Instance.Hide();
                await UniTask.Delay(TimeSpan.FromSeconds(1f));
            }
        }

        MasterControl.Instance.ShowRewardedAd(callback, checkNoAds);
    }


    public void LogUserWatchAdsToUnlock(DataMonster data)
    {
        if (!PrefAccess.IsWatchedAdsToUnlockMonster(data)) return;
        FirebaseManager.Instance.LogEvent($"REWARD_UNLOCK_{data.id}");
        PrefAccess.SetWatchedAdsToUnlock(data);
    }

    #endregion

    #region Toggle Function

    public void ToggleVibration()
    {
        vibrationOn = !vibrationOn;
        PlayerPrefs.SetInt("Vibration", vibrationOn ? 1 : 0);
    }

    public void ToggleFlash()
    {
        flashOn = !flashOn;
        PlayerPrefs.SetInt("Flash", flashOn ? 1 : 0);
    }

    #endregion

    #region Intent

    public bool CheckInternet()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            return false;
        }

        return true;
    }

    public void OpenWifiSetting()
    {
        try
        {
            AndroidJavaObject intent = new AndroidJavaObject("android.content.Intent");
            intent.Call<AndroidJavaObject>("setAction", "android.settings.WIFI_SETTINGS");

            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            currentActivity.Call("startActivity", intent);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    #endregion

    [Button]
    public async void HideBanner()
    {
        for (int i = 0; i < 5; i++)
        {
            try
            {
                MasterControl.Instance.HideBanner();
            }
            catch (Exception e)
            {
                // ignored
            }

            await UniTask.Delay(TimeSpan.FromSeconds(.1f));
        }
    }

    #region Debug ads

    public void OnButtonClick()
    {
        MobileAds.OpenAdInspector(error => { });
    }

    #endregion

#if UNITY_EDITOR
    public List<Sprite> sample;

    [Button]
    public void GetListName()
    {
        string res = "";
        foreach (Sprite sprite in sample)
        {
            res += sprite.name + ",";
        }

        Debug.Log(res);
    }
#endif
}
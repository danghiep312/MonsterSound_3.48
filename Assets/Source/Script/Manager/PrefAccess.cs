using UnityEngine;


public class PrefAccess
{
    public static bool CheckMonsterUnlocked(DataMonster data)
    {
        return PlayerPrefs.GetInt("Monster" + data.id, 0) >= data.adToUnlock;
    }
    
    public static void WatchAdsToUnlock(DataMonster data)
    {
        PlayerPrefs.SetInt("Monster" + data.id, PlayerPrefs.GetInt("Monster" + data.id, 0) + 1);
    }
    
    public static int GetNumOfAdsWatched(DataMonster data)
    {
        return PlayerPrefs.GetInt("Monster" + data.id, 0);
    }

    public static bool IsWatchedAdsToUnlockMonster(DataMonster data)
    {
        return PlayerPrefs.GetInt("WatchAdsToUnlock" + data.id, 0) == 1;
    }
    
    public static void SetWatchedAdsToUnlock(DataMonster data)
    {
        PlayerPrefs.SetInt("WatchAdsToUnlock" + data.id, 1);
    }
    
    public static bool FirstTimePlay()
    {
        return PlayerPrefs.GetInt("FirstTimePlay", 0) == 0;
    }
}

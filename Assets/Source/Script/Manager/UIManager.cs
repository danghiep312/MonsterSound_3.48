using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;


public class UIManager : Singleton<UIManager>
{
    public GameObject internetPanel;
    public GameObject currentHUD;
    public GameObject settingPanel;
    
    [Space]
    [InfoBox("Index of hud array\n1. Home\n2. Gameplay")]
    public GameObject[] Huds;
    
    
    private void Start()
    {
        
        this.RegisterListener(EventID.GoToPlay, (param) => GoToPlay((DataMonster)param));
        //ChangeHUD("Menu");
        //Util.ActivateLastFrame(Init);
        
    }

    private void GoToPlay(DataMonster dat)
    {
        GetHud("Gameplay").GetComponent<GameplayUI>().Setup(dat);
        ChangeHUD("Gameplay");
    }

    private void Init()
    {
        //Array.ForEach(Huds, o => o.SetActive(false));
        //ChangeHUD("Menu");
    }

    public void ChangeHUD(string hudName)
    {
        try
        {
            if (currentHUD != null) currentHUD.SetActive(false);
            currentHUD = GetHud(hudName);
            if (currentHUD) currentHUD.SetActive(true);
            else Common.LogWarning(this, "Not found HUD: " + hudName);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        } 
    }

    private GameObject GetHud(string hudName)
    {
        return Huds.FirstOrDefault(t => t.name == hudName);
    }

    public void BackToMenu()
    {
        GameController.Instance.ShowInter(mustRunCallback:BackProcess);
    }

    private void BackProcess()
    {
        ChangeHUD("Menu");
        this.PostEvent(EventID.BackToMenu);
        PlayerPrefs.SetInt("FirstTimePlay", 1);
    }

    public void OpenSetting()
    {
        this.PostEvent(EventID.OpenSetting);
        settingPanel.SetActive(true);
    }

    public void SetInternetPanel(bool status)
    {
        try
        {
            internetPanel.SetActive(status);
        }
        catch 
        {
            // ignored
        }
    }
}



// using System;
// using System.Linq;
// using System.Threading.Tasks;
// using Sirenix.OdinInspector;
// using UnityEditor;
// using UnityEngine;
// using UnityEngine.AddressableAssets;
// using UnityEngine.ResourceManagement.AsyncOperations;
// using UnityEngine.Serialization;
//
//
// public class UIManager : Singleton<UIManager>
// {
//     public GameObject internetPanel;
//     public GameObject currentHUD;
//     public GameObject settingPanel;
//
//     public GameObject home;
//
//     [Space] [InfoBox("Index of hud array\n1. Home\n2. Menu\n3. Gameplay\n4. Quiz")]
//     public GameObject[] Huds;
//
//
//     private void Start()
//     {
//
//         this.RegisterListener(EventID.GoToPlay, (param) => GoToPlay((DataMonster)param));
//         //currentHUD = GetHud("Home").Result;
//         //ChangeHUD("Menu");
//         //Util.ActivateLastFrame(Init);
//
//     }
//
//     private void GoToPlay(DataMonster dat)
//     {
//         //GetHud("Gameplay").Result.GetComponent<GameplayUI>().Setup(dat);
//         ChangeHUD("Gameplay", dat);
//     }
//
//     private void Init()
//     {
//         //Array.ForEach(Huds, o => o.SetActive(false));
//         //ChangeHUD("Menu");
//     }
//
//     public async void ChangeHUD(string hudName, DataMonster dat = null)
//     {
//         LoadingTransition.Instance.Show();
//
//         async void Action()
//         {
//             if (!currentHUD.name.Equals("Home"))
//             {
//                 Addressables.ReleaseInstance(currentHUD);
//             }
//             else
//             {
//                 currentHUD.SetActive(false);
//             }
//
//             if (hudName.Equals("Home") || hudName.Equals(""))
//             {
//                 currentHUD = home;
//                 currentHUD.SetActive(true);
//             }
//             else
//             {
//                 var hud = GetHud(hudName);
//                 await hud;
//                 if (dat != null) hud.Result.GetComponent<GameplayUI>().Setup(dat);
//                 currentHUD = hud.Result;
//             }
//
//             Util.Delay(.5f, () => LoadingTransition.Instance.Hide());
//         }
//
//         Util.Delay(LoadingTransition.Instance.elapsedTime, Action);
//         // try
//         // {
//         //     if (currentHUD != null) currentHUD.SetActive(false);
//         //     currentHUD = GetHud(hudName);
//         //     if (currentHUD) currentHUD.SetActive(true);
//         //     else Common.LogWarning(this, "Not found HUD: " + hudName);
//         // }
//         // catch (Exception e)
//         // {
//         //     Debug.Log(e);
//         // } 
//     }
//
//     private async Task<GameObject> GetHud(string hudName)
//     {
//         //return Huds.FirstOrDefault(t => t.name == hudName);
//         AsyncOperationHandle<GameObject> hud = Addressables.InstantiateAsync(hudName, transform);
//         await hud.Task;
//         return hud.Result;
//     }
//
//     public void BackToMenu()
//     {
//         GameManager.Instance.ShowInter(mustRunCallback: BackProcess);
//     }
//
//     private void BackProcess()
//     {
//         ChangeHUD("Menu");
//         this.PostEvent(EventID.BackToMenu);
//         PlayerPrefs.SetInt("FirstTimePlay", 1);
//     }
//
//     public void OpenSetting()
//     {
//         this.PostEvent(EventID.OpenSetting);
//         settingPanel.SetActive(true);
//     }
//
//     public void SetInternetPanel(bool status)
//     {
//         try
//         {
//             internetPanel.SetActive(status);
//         }
//         catch
//         {
//             // ignored
//         }
//     }
//
//     public void ClickPrank()
//     {
//         ChangeHUD("Menu");
//     }
//
//     public void BackToHome()
//     {
//         ChangeHUD("Home");
//     }
//
//     public void ClickQuiz()
//     {
//         ChangeHUD("Quiz");
//     }
// }

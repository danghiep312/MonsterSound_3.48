﻿using System;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.UI;

public class BackToMonsterVoice : MonoBehaviour
{
    private Button _button;

    private void Start()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        LoadingTransition.Instance.Show();
        AsyncOperationHandle<SceneInstance> handle = Addressables.LoadSceneAsync("Main", activateOnLoad:false);
        handle.Completed += (op) =>
        {
            Util.Delay(LoadingTransition.Instance.elapsedTime *2f, () =>
            {
                op.Result.ActivateAsync();
            });
        };
    }
}

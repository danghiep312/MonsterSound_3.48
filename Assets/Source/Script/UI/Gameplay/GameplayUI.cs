using System.Collections;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using SourceAds;
using UnityEngine.SceneManagement;

public class GameplayUI : MonoBehaviour
{
    public Toggle isPlaying;
    [SerializeField] private Image monster;
    [SerializeField] private TextMeshProUGUI monsterName;
    [SerializeField] private Toggle loop;
    [SerializeField] private Dropdown timer;
    [SerializeField] private TextMeshProUGUI cdText;
    [SerializeField] private AudioSource currentAudio;
    [SerializeField] private ParticleSystem wave;

    
    private float audioLength;
    private float audioLengthLeft;
    
    // public NativeAdLarge largeNativeAds;
    private DataMonster _currentMonster;
    public int[] adsWatched;
    private readonly int[] countdownTime = { 0, 15, 30, 45, 60, 120, 300 };

    private float timeRemain;
    private bool countdown;
    private float cooldownHaptic = 1f;

    #region Init
    
    private void Awake()
    {
        this.RegisterListener(EventID.BackToMenu, (param) => OnBackToMenu());
        this.RegisterListener(EventID.OpenSetting, (param) => OnSettingOpened());
        this.RegisterListener(EventID.CloseSetting, (param) => OnSettingClosed());
        //gameObject.SetActive(false);
    }

    private void Start()
    {
        isPlaying = GameObject.FindWithTag("PlayToggle").GetComponent<Toggle>();
        isPlaying.onValueChanged.AddListener(OnValueChanged);
        timer.onValueChanged.AddListener(OnChangedTimer);
        loop.onValueChanged.AddListener(OnChangedLoop);
    }
    
    private void OnEnable()
    {
        adsWatched = new[] { 1, 0, 0, 0, 1, 1, 1 };
        //largeNativeAds.isAllowShow = true;
        if (!PrefAccess.FirstTimePlay())
        {
            // largeNativeAds.TryShow();
            MasterControl.Instance.ShowBanner();
        }
    }

    public void Setup(DataMonster data)
    {
        Debug.Log(data.id);
        _currentMonster = data;
        monster.sprite = data.mainMonsterSprite;
        monsterName.text = data.monsterName;
        timer.value = 0;
        
#if !UNITY_IOS
        currentAudio.clip = data.iosClip;
        if (currentAudio.clip == null) currentAudio.clip = data.clip;
#elif UNITY_IOS
        currentAudio.clip = data.iosClip;
        if (currentAudio.clip == null) currentAudio.clip = data.clip;
#endif
        
        if (!PrefAccess.FirstTimePlay())
        {
            var logMessage = "PLAY_MONSTER_" + data.id;
            //if (SceneManager.GetActiveScene().name.Equals("SubMain")) logMessage += "_SKI";
            FirebaseManager.Instance.LogEvent(logMessage);
        }
        else
        {
            LoadingTransition.Instance.Hide();
        }
        audioLength = data.clip.length;
    }
    
    #endregion
    
    #region Handle Event
    private void OnChangedLoop(bool arg0)
    {
        if (loop.isOn)
        {
            OffPlay();
        }

        currentAudio.loop = loop.isOn;
    }
    
    private void OnChangedTimer(int index)
    {
        OffPlay();
        //index = timer.value;
        if (index == 0)
        {
            countdown = false;
            cdText.gameObject.SetActive(false);
            return;
        }

        if (index < 4 && adsWatched[index] == 0)
        {
#if UNITY_EDITOR
            RewardCallback(index);
            return;
#endif
            GameController.Instance.ShowReward(callback: (success) =>
            {
                if (success || !PrefInfo.IsUsingAd()) RewardCallback(index);
            });
        }
        else
        {
            RewardCallback(index);
        }
    }
    
    private void OnValueChanged(bool value)
    {
        Common.Log("Reverse IsPlaying");
        if (isPlaying.isOn)
        {
            this.PostEvent(EventID.Play);
            MonsterIdle(true);
            SetParticleStatus(true);
            currentAudio.Play();
            audioLengthLeft = audioLength;
            if (!currentAudio)
            {
                Common.LogWarning(this, "Audio is null");
                return;
            }
            //StartCoroutine(CheckCanReplay(audioLength));
        }
        else
        {
            StopAllCoroutines();
            MonsterIdle(false);
            SetParticleStatus(false);
            AudiosManager.Instance.StopAll();
            currentAudio.Stop();
        }
    }
    
    private void OnSettingOpened()
    {
        OffPlay();
        // largeNativeAds.gameObject.SetActive(false);
        MasterControl.Instance.HideBanner();
    }

    private void OnSettingClosed()
    {
        if (!gameObject.activeSelf) return;
        // largeNativeAds.TryShow();
        MasterControl.Instance.ShowBanner();
    }
    
    private void OnBackToMenu()
    {
        OffPlay();
        timer.value = 0;
        countdown = false;
        AudiosManager.Instance.StopAll();
    }

    public void BackToMenu()
    {
        UIManager.Instance.BackToMenu();
    }

    private void OffPlay()
    {
        isPlaying.isOn = false;
    }

    private void OnPlay()
    {
        isPlaying.isOn = true;
    }
#endregion

    public void TogglePlay()
    {
        isPlaying.isOn = !isPlaying.isOn;
    }

    private void Update()
    {
        if (isPlaying.isOn)
        {
            HapticHandle();
            PlayingHandle();
        }
        

        if (!countdown) return;
        timeRemain -= Time.deltaTime;
        cdText.text = "The sound will play in " + FormatTime(timeRemain);
        if (timeRemain < 0)
        {
            countdown = false;
            cdText.gameObject.SetActive(false);
            PlayWhenCountdownComplete();
        }
    }

    #region Handle Haptic
    
    private void HapticHandle()
    {
        if (cooldownHaptic > 0) cooldownHaptic -= Time.deltaTime;
        else
        {
            Haptic();
            cooldownHaptic = 1f;
        }
    }
    
    private void Haptic()
    {
       
        if (GameController.Instance.flashOn)
        {
            //Util.Flashing(.5f);
        }

        if (GameController.Instance.vibrationOn)
        {
            Util.Vibrate(() => MMVibrationManager.ContinuousHaptic(0.1f, 0.05f, .5f));
        }
    }
    

    #endregion

    #region Handle Playing
    private void PlayingHandle()
    {
        if (audioLengthLeft > 0) audioLengthLeft -= Time.deltaTime;
        else
        {
            CheckCanPlay();
            audioLengthLeft = audioLength;
        }
    }

    private void CheckCanPlay()
    {
        if (!loop.isOn)
        {
            OffPlay();
        }
        else
        {
            
        }
    }

    private void PlayWhenCountdownComplete()
    {
        OffPlay();
        Util.Delay(.2f, OnPlay);
    }

    #endregion

    private void RewardCallback(int index)
    {
        adsWatched[index] = 1;
        LogTimeEvent(index);
        cdText.gameObject.SetActive(true);
        timeRemain = countdownTime[index];
        cdText.text = "The sound will play in " + FormatTime(timeRemain);
        countdown = true;
    }

    private void LogTimeEvent(int index)
    {
        if (index == 0) return;
        int timeEvent = countdownTime[index];
        if (timeEvent > 60)
        {
            timeEvent /= 60;
            FirebaseManager.Instance.LogEvent($"TIMER_{timeEvent}M_{_currentMonster.id}");
        }
        FirebaseManager.Instance.LogEvent($"TIMER_{timeEvent}S_{_currentMonster.id}");
    }
    
    private string FormatTime(float time)
    {
        int minute = (int) time / 60;
        int second = (int) time % 60;
        return $"{minute:00}:{second:00}";
    }

    private void MonsterIdle(bool status)
    {
        if (status)
            monster.transform.DOScale(Vector3.one * 1.2f, 0.5f).SetEase(Ease.InOutSine)
                .SetLoops(-1, LoopType.Yoyo);
        else
        {
            monster.transform.DOKill();
            monster.transform.localScale = Vector3.one;
        }
    }

    public void SetParticleStatus(bool status)
    {
        if (!status)
        {
            wave.Clear();
            wave.Stop();
        }
        Debug.Log("particle " + status);
        wave.gameObject.SetActive(status);
        if (status)
        {
            wave.Play();
        }
    }
}

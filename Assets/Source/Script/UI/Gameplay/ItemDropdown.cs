using System;
using System.Linq;
using UnityEngine;


public class ItemDropdown : MonoBehaviour
{
    public GameObject adIcon;

    private int[] adIndex= {1, 2, 3};
    
    private void Start()
    {
        var adsWatched = GetComponentInParent<GameplayUI>().adsWatched;
        if (adIndex.Any(index => gameObject.name.Contains("Item " + index) && adsWatched[index] == 0) && PrefInfo.IsUsingAd())
        {
            adIcon.SetActive(true);
        }
    }
    
    
}

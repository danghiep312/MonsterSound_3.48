using System;
using UnityEngine;
using UnityEngine.UI;


public class LoopButton : MonoBehaviour
{
    [SerializeField] private Toggle loop;
    [SerializeField] private GameObject lockObj;
    [SerializeField] private GameObject adIcon;
    private bool isUnlocked;
    
    

    private void OnEnable()
    {
        loop.isOn = false;
        isUnlocked = false;
        lockObj.SetActive(!isUnlocked);
        if (!PrefInfo.IsUsingAd())
        {
            isUnlocked = true;
            lockObj.SetActive(!isUnlocked);
            adIcon.SetActive(false);
        }
    }
    
    public void ToggleLoop()
    {
        if (!isUnlocked)
        {
            GetComponentInParent<GameplayUI>().isPlaying.isOn = false;
            GameController.Instance.ShowReward(RewardCallback, true);
#if UNITY_EDITOR
            RewardCallback(true);
#endif
        }
        else
        {
            loop.isOn = !loop.isOn;
        }
    }
    
    private void RewardCallback(bool success)
    {
        if (success)
        {
            isUnlocked = true;
            lockObj.SetActive(!isUnlocked);
        }
    }
}

using System;
using TMPro;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;

public class VolumeDetect : MonoBehaviour
{
     private string audioName;
     public Slider slider;
     public TextMeshProUGUI text;
     
     
     
     private void Start()
     {
         slider = GetComponent<Slider>();
         ModifySliderValue();
     }
     
     private void ModifySliderValue()
     {
         slider.value = GetVolume();
         text.text = (int)(slider.value * 100) + "";
     }

     private float GetVolume()
     {
#if UNITY_EDITOR
        return .5f;
#endif
        AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
        audioName = currentActivity.GetStatic<string>("AUDIO_SERVICE");
        
        AndroidJavaObject audioManager = currentActivity.Call<AndroidJavaObject>("getSystemService", audioName);
        
        // Get the current volume for the music stream
        int currentVolume = audioManager.Call<int>("getStreamVolume", 3); // 3 is the constant value for AudioManager.STREAM_MUSIC
        int maxVolume = audioManager.Call<int>("getStreamMaxVolume", 3); // 3 is the constant value for AudioManager.STREAM_MUSIC
        return currentVolume * 1f / maxVolume;
    }

    private void Update()
    {
        //ModifySliderValue();
    }
}
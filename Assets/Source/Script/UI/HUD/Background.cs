using System;
using UnityEngine;


public class Background : MonoBehaviour
{
    private SpriteRenderer _sr;
    public Sprite lightBackground;
    public Sprite nightBackground;

    private void Start()
    {
        _sr = GetComponent<SpriteRenderer>();
        this.RegisterListener(EventID.GoToPlay, (param) => GoToPlay());
        this.RegisterListener(EventID.BackToMenu, (param) => BackToMenu());
    }

    private void BackToMenu()
    {
        _sr.sprite = nightBackground;
    }

    private void GoToPlay()
    {
        _sr.sprite = lightBackground;
    }
}

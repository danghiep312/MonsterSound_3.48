using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideIAP : MonoBehaviour
{
    public bool isPurchased;
    private Button _btn;

    private void Start()
    {
        _btn = GetComponent<Button>();
        _btn.onClick.AddListener(() =>
        {
            MasterControl.Instance.adsController._allowShowOpenAds = false;
        });
        isPurchased = !PrefInfo.IsUsingAd();
        if (isPurchased)
        {
            gameObject.SetActive(false);
        }
    }

    private void OnEnable()
    {
        if (isPurchased)
        {
            gameObject.SetActive(false);
        }
    }
}

using System;
using UnityEngine;
using UnityEngine.Purchasing;
using SourceAds;

public class IAPButtonCustom : MonoBehaviour
{
    private IAPButton _button;
    private MasterControlListener _listener;
    
    private void Start()
    {
        _button = GetComponent<IAPButton>();
        if (_button == null)
        {
            Common.LogWarning(this, "Component IAP Button not found");
            return;
        }

        _listener = FindObjectOfType<MasterControlListener>(true);

        if (_listener == null)
        {
            Common.LogWarning(this, "Listener not found");
            return;
        } 
        
        _button.onPurchaseComplete.AddListener(_listener.OnPurchased);
        _button.onPurchaseFailed.AddListener(_listener.OnFailedToPurchase);
    }
}

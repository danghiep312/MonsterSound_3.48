using System;
using System.Collections;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Loading : MonoBehaviour
{
    public Image transition;
    public Image logo;
    public Image fill;
    public TextMeshProUGUI text;
    private readonly string scenePath = "Main";
    private WaitForSeconds _delay = new WaitForSeconds(0.25f);

    private void Start()
    {
        LoadScene(scenePath);
        transition.DOFade(0, .3f).SetEase(Ease.Linear).OnComplete(() =>
        {
            logo.rectTransform.anchoredPosition = Vector2.up * 100f;
            logo.DOFade(1, .5f).SetEase(Ease.Linear);
            logo.rectTransform.DOAnchorPosY(350f, .5f).SetEase(Ease.OutQuad);
        });
    }

    private void Update()
    {
        //text.text = $"{Mathf.RoundToInt(fill.fillAmount * 100)}%";
        if (text.text.Equals("Loading"))
        {
            StartCoroutine(TextLoading());
        }
    }
    

    private IEnumerator TextLoading()
    {
        yield return _delay;
        text.text = "Loading.";
        yield return _delay;
        text.text = "Loading..";
        yield return _delay;
        text.text = "Loading...";
        yield return _delay;
        text.text = "Loading";
    }
    
    private async void LoadScene(string sceneKey)
    {
        AsyncOperationHandle<SceneInstance> handle = Addressables.LoadSceneAsync(sceneKey, activateOnLoad:false);
        var fakeProgress = 0.0f;
        fill.fillAmount = 0.0f;
 
        // Start loading scene but not activate it
        // var scene = SceneManager.LoadSceneAsync(sceneName);
        // scene.allowSceneActivation = false;

        do
        {
            await UniTask.Delay(200); // Timeout between the 'ticks' of progress bar
            fakeProgress += Random.Range(0.01f, 0.1f); // Value of one 'tick'
            //_realProgress = scene.progress;
//            Debug.Log(_fakeProgress + " " + _realProgress);
            fill.DOKill();
            fill.DOFillAmount(Mathf.Min(fakeProgress, handle.PercentComplete) / .9f, .2f).SetEase(Ease.OutSine);
        } while (fakeProgress < 0.9f );
        // Using Random we can have the progress between 0.91 and 1.0 in the end of loop...

        //...so, set the progress bar value to 100%
        fill.fillAmount = 1f;
        await handle;
        
        //Debug.Log("In here");
        while ((!MasterControl.Instance.IsReady() && Time.time < 10f))
        {
            await UniTask.Yield();
        }

        async void ActiveScene(bool b)
        {
            //scene.allowSceneActivation = true;// Now, activate the scene
            
            await handle.Result.ActivateAsync();
        }
        
        MasterControl.Instance.ShowOpenAd((ActiveScene));
    }
    
    
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class ButtonHolder : MonoBehaviour
{
    //public DataMonster[] dataMonsters;
    public int[] adIndex;
    private List<NativeAdButton> nativeAds = new List<NativeAdButton>();
    private List<DataMonster> dataMonsters;
    
    public UnityEvent getData;
    public AssetReference monPrefab, adPrefab;
    public Transform skibidiVoice;
    
    private void OnEnable()
    {
        try
        {
            if (nativeAds.Count <= 0) return;
            foreach (var ad in nativeAds) ad.TryShow();
        }
        catch
        {
            // ignored
        }
    }
    

    private void Start()
    {
        getData.Invoke();
        nativeAds = new List<NativeAdButton>();
        Init();
        Util.ActivateLastFrame(() =>
        {
            var size = CalculateCellSize(GetComponent<RectTransform>());
            GetComponentInChildren<GridLayoutGroup>().cellSize = size;
        });
    }
    
    
    private void Init()
    {
        Addressables.LoadAssetAsync<GameObject>(monPrefab).Completed += MonsterLoadComplete;
        
    }
    
    private void MonsterLoadComplete(AsyncOperationHandle<GameObject> obj)
    {
        Debug.Log("Exclude: " + PlayerPrefs.GetString(CustomPattern.ExcludeIdMonster, "-1"));
        int[] excludeId = Array.ConvertAll(PlayerPrefs.GetString(CustomPattern.ExcludeIdMonster, "-1").Split(","), int.Parse);
        Transform grid = transform.GetChild(0).GetChild(0);
        if (obj.Status == AsyncOperationStatus.Succeeded)
        {
            foreach (var t in dataMonsters)
            {
                if (excludeId.Contains(t.id)) continue;
                GameObject bt = Instantiate(obj.Result, grid);
                bt.GetComponent<MonsterButton>().Init(t);
            }
        }
        if (skibidiVoice)
        {
            skibidiVoice.SetSiblingIndex(1);
        }
        Addressables.LoadAssetAsync<GameObject>(adPrefab).Completed += AdButtonLoadComplete;
    }

    private void AdButtonLoadComplete(AsyncOperationHandle<GameObject> obj)
    {
        Transform grid = transform.GetChild(0).GetChild(0);
        if (obj.Status == AsyncOperationStatus.Succeeded)
        {
            foreach (int index in adIndex)
            {
                GameObject ad = Instantiate(obj.Result, grid);
                ad.name = "Native " + index;
                nativeAds.Add(ad.GetComponent<NativeAdButton>());
            }
        }
        SetIndexForAdButton();
        
        if (PrefAccess.FirstTimePlay())
        {
            grid.GetChild(0).GetComponent<MonsterButton>().ButtonPressed();
            transform.parent.gameObject.SetActive(false);
        }
        else
        {
            Util.ActivateLastFrame(LoadingTransition.Instance.Hide);
        }
    }

    private void SetIndexForAdButton()
    {
        for (int i = 0; i < nativeAds.Count; i++)
        {
            nativeAds[i].transform.SetSiblingIndex(adIndex[i]);
        }
    }

    private Vector2 CalculateCellSize(RectTransform container)
    {
        var width = (container.rect.width - GetComponentInChildren<GridLayoutGroup>().spacing.x) / 2;
        var height = width * 350f / 350f; // 350 x 350 is the size of the sprite
        return new Vector2(width, height);
    }

    public void GetMonstersData()
    {
        dataMonsters = Database.Instance.GetMonstersData();
    }
    

}
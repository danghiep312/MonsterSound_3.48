﻿using System;
using UnityEngine;
using UnityEngine.UI;


public class MenuUI : MonoBehaviour
{
    public Button back;

    private void Start()
    {
        back.onClick.AddListener(OnBack);
    }

    private void OnBack()
    {
        //UIManager.Instance.BackToHome();
    }
}

using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using SourceAds;

public class MonsterButton : MonoBehaviour
{
    public TextMeshProUGUI monsterName;
    public TextMeshProUGUI adsWatched;
    public Image monsterIcon;
    public GameObject lockObj;

    private Image _image;
    private DataMonster _data;

    private bool _isUnlocked;
    private Button _button;

    private void OnEnable()
    {
        if (!_button) _button = GetComponent<Button>();
        if (_data)
        {
            CheckIsUnlockedMonster(_data);
        }
        _button.interactable = false;
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, .3f).SetEase(Ease.OutBack).OnComplete(() => _button.interactable = true);
    }

    public void Init(DataMonster data)
    {
        // try
        // {
        //     Debug.Log(data.iconMonsterSprite + " sprite");
        //     Debug.Log(data.iconMonsterSprite.name + " sprite name");
        //     Debug.Log(lockObj.transform.GetChild(0).GetComponent<Image>().name + " sprite name");
        // }
        // catch (Exception e)
        // {
        //     Debug.Log("Error: " + e + " " + data.id);
        // }
        _image = GetComponent<Image>();
        _data = data;
        monsterName.text = data.monsterName;
        monsterName.color = data.textColor;
        monsterIcon.sprite = data.iconMonsterSprite;
        _image.sprite = data.cardMonsterSprite;
        //GetComponent<Button>().onClick.AddListener(ButtonPressed);
        CheckIsUnlockedMonster(_data);
        adsWatched.text = "UNLOCK (" + PrefAccess.GetNumOfAdsWatched(data) + "/" + data.adToUnlock + ")";
    }

    public void ButtonPressed()
    {
        if (_isUnlocked)
        {
            //GameManager.Instance.HideBanner();
            //GameManager.Instance.SetState(State.Gameplay);
            GameController.Instance.ShowInter(mustRunCallback: () => this.PostEvent(EventID.GoToPlay, _data));
        }
        else
        {
#if UNITY_EDITOR
            OnRewardSuccess();
            return;
#endif
            GameController.Instance.ShowReward((param) =>
            {
                if (!param) return;
                OnRewardSuccess();
            });
        }
    }

    private void OnRewardSuccess()
    {
        GameController.Instance.LogUserWatchAdsToUnlock(_data);
        FirebaseManager.Instance.LogEvent($"REWARD_COUNT_UNLOCK");
        PrefAccess.WatchAdsToUnlock(_data);
        if (CheckIsUnlockedMonster(_data))
        {
            this.PostEvent(EventID.GoToPlay, _data);
        }
        adsWatched.text = "UNLOCK (" + PrefAccess.GetNumOfAdsWatched(_data) + "/" + _data.adToUnlock + ")";
    }

    private bool CheckIsUnlockedMonster(DataMonster data)
    {
        _isUnlocked = PrefAccess.CheckMonsterUnlocked(data);
        if (!PrefInfo.IsUsingAd())
        {
            _isUnlocked = true;
        }
        SetLockStatus(_isUnlocked);
        return _isUnlocked;
    }

    private void SetLockStatus(bool status)
    {

        lockObj.SetActive(!status);
    }
}

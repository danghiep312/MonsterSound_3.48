﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.UI;

public class SkiButton : MonoBehaviour
{
    private Button _button;


    private void OnEnable()
    {
        if (!_button) _button = GetComponent<Button>();
        _button.interactable = false;
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, .3f).SetEase(Ease.OutBack).OnComplete(() => _button.interactable = true);
    }

    private void Start()
    {
        _button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        LoadScene();        
    }
    
    private void LoadScene()
    {
        LoadingTransition.Instance.Show();
        AsyncOperationHandle<SceneInstance> handle = Addressables.LoadSceneAsync("SubMain", activateOnLoad:false);
        handle.Completed += (op) =>
        {
            Util.Delay(LoadingTransition.Instance.elapsedTime * 2f, () =>
            {
                op.Result.ActivateAsync();
            });
        };
    }
}
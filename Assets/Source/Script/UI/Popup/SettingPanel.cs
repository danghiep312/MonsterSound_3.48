using System;
using UnityEngine;
using SourceAds;


public class SettingPanel : Panel
{
    private void OnEnable()
    {
        FirebaseManager.Instance.LogEvent("SETTING");
    }

    private void OnDisable()
    {
        this.PostEvent(EventID.CloseSetting);
    }
    
    public void RestorePurchase()
    {
        MasterControl.Instance.CheckRestore();
    }

    public override void PostInit()
    {
        
    }

    public override void Setup(object o)
    {
        
    }
}

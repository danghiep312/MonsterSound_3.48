using System;
using UnityEngine;
using UnityEngine.UI;


public class SettingButtonSprite : MonoBehaviour
{
    private Image _image;
    [Tooltip("0 is off, 1 is on")]
    public Sprite[] buttonSprite;

    private bool _status;

    private void Awake()
    {
        _image = GetComponent<Image>();
        Update();
    }

    private void Update()
    {
        _status = gameObject.name switch
        {
            "Vibration" => GameController.Instance.vibrationOn,
            "Flash" => GameController.Instance.flashOn,
            _ => true
        };

        _image.sprite = buttonSprite[_status ? 1 : 0];
    }
}
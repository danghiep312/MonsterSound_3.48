using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;


public class Tutorial : MonoBehaviour
{
    [Title("For Tutorial")] 
    public Transform playButton;
    public Transform backButton;
    public Transform tutorialBack;
    public Transform tutorialPanel;
    public GameObject tutorialText;
    public Transform header;
    public Toggle IsPlaying;

    private void Start()
    {
        IsPlaying = GameObject.FindWithTag("PlayToggle").GetComponent<Toggle>();
        if (!PrefAccess.FirstTimePlay())
        {
            GetComponent<Tutorial>().enabled = false;
            return;
        }
        this.RegisterListener(EventID.BackToMenu, o => OnBackToMenu());
        IsPlaying.onValueChanged.AddListener(OnValueChanged);
    }

    private void OnBackToMenu()
    {
        backButton.SetParent(header);
        header.SetAsLastSibling();
        tutorialPanel.gameObject.SetActive(false);
        tutorialBack.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        if (PrefAccess.FirstTimePlay())
        {
            header.SetAsFirstSibling();
            tutorialPanel.gameObject.SetActive(true);
            tutorialText.SetActive(true);
            playButton.SetSiblingIndex(transform.childCount - 1);
        }
    }
    
    private void OnValueChanged(bool b)
    {
        if (b) return;
        if (PrefAccess.FirstTimePlay())
        {
            
            playButton.SetSiblingIndex(0);
            tutorialPanel.SetSiblingIndex(transform.childCount - 1);
            tutorialBack.gameObject.SetActive(true);
            backButton.SetParent(transform);
            
        }
    }
}
